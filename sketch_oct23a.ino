#define PWMA 11 
#define PWMB 10
#define AIN1 6
#define AIN2 7
#define BIN1 5
#define BIN2 4
#define STBY 13 



void setup(){

    pinMode(PWMA, OUTPUT);  
    pinMode(PWMB, OUTPUT);  
    pinMode(AIN1, OUTPUT);  
    pinMode(AIN2, OUTPUT);  
    pinMode(BIN1, OUTPUT);  
    pinMode(BIN2, OUTPUT);  
    pinMode(STBY, OUTPUT);  

    digitalWrite(STBY, HIGH); 
 
}
void loop(){
move(1, 255, 1); 
move(2, 255, 1); 

delay(1000);
stop(); //stop
delay(250); 

move(1, 128, 0); 
move(2, 128, 0); 

delay(1000);
stop();
delay(250);
}

void move(int motor, int speed, int direction){

digitalWrite(STBY, HIGH); 
boolean inPin1 = LOW;
boolean inPin2 = HIGH;

if(direction == 1){
inPin1 = HIGH;
inPin2 = LOW;
}

if(motor == 1){
digitalWrite(AIN1, inPin1);
digitalWrite(AIN2, inPin2);
analogWrite(PWMA, speed);
}else{
digitalWrite(BIN1, inPin1);
digitalWrite(BIN2, inPin2);
analogWrite(PWMB, speed);
}
}

void stop(){
digitalWrite(STBY, LOW);
}
